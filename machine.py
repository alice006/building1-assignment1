from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen, ScreenManager, FadeTransition
from kivy.properties import ObjectProperty, ListProperty

class ScreenManagement(ScreenManager):
    pass

class Machine(Screen):
    list_item = ["Fresh Milk","Super Milk","Yogurt","Premium Milk","Sheep Milk","Orange Juice"]
    list_cost = [9,12,8,17,25,19]
    list_amount = [15,15,15,15,15,15]
    list_sell_item = [0,0,0,0,0,0]
    coin = [200,200,0]
    list_name_item = ["milk1","milk2","milk3","milk5","milk6","orange1"]
    coin_pay = [0,0,0]
    item_choose = ""
    pointer_itemsold = 0
    totalpay = 0
    change = 0
    state = 0
    user_paid = 0
    price = 0
    showcoin = [0,0]
    history_set_list = ["","","","","","","","","","","","","","",""]
    check_pos = 0
    
    def pickitem(self,itemtype):
        self.showcoin = [0,0]
        if self.state <= 1 :
            self.count=itemtype
            if(self.list_amount[self.count]>0):
                self.state = 1
                self.pointer_itemsold = self.count
                self.totalpay = self.list_cost[self.count]
                self.item_choose = self.list_item[self.count]
            else:
                self.cancel()
                self.item_choose = "Sold Out"
        self.showtext()
        self.status()
        
    def paycoin(self,cointype):
        if(self.state>=1 and self.state<3):
            self.state = 2
            if(cointype==1):
                self.user_paid += 1
                self.coin_pay[0] += 1
            elif(cointype==5):
                self.user_paid += 5
                self.coin_pay[1] += 1
            elif(cointype==10):
                self.user_paid += 10
                self.coin_pay[2] += 1
        while(self.state == 2 and self.user_paid>=self.totalpay):
            self.state=3
            self.picture = self.list_name_item[self.pointer_itemsold]
            self.change = self.user_paid - self.totalpay
            break
        self.showtext()
        self.status()
        
    def cancel(self):
        if(self.state<=2):
            self.state = 0
            self.totalpay = 0
            self.change = 0
            self.item_choose = "None Choose"
            self.user_paid = 0
            self.pointer_itemsold = 0
            for i in range(3):
                self.coin_pay[i]=0
            self.picture = ""
        self.showtext()
        self.status()

    def getchange(self):
        if(self.state==3):
            for i in range(3):
                self.coin[i] += self.coin_pay[i]
            if(self.change<5):
                if(self.coin[0]<self.change):
                    self.change -= self.coin[0]
                    self.coin[0] = 0
                    self.showcoin[0] = self.change
                else:
                    self.coin[0] -= self.change
                    self.showcoin[0] = self.change
                    self.change = 0
            elif(self.change>5 and self.coin[1]>1):
                self.coin[1] -= 1
                self.coin[0] -= self.change%5
                self.showcoin[1] = 1
                self.showcoin[0] = self.change%5
                self.change = 0
            elif(self.change>5 and self.coin[1]==0):
                if(self.coin[0]>=self.change):
                    self.coin[0] -= self.change
                    self.showcoin[0] = self.change
                    self.change = 0
                else:
                    self.change -= self.coin[0]
                    self.showcoin[0] = self.change
                    self.coin[0] = 0
            elif(self.change==5 and self.coin[1]>=1):
                self.change = 0
                self.showcoin[1] = 1
                self.coin[1] -= 1
            elif(self.change==5 and self.coin[1]==0):
                if(self.coin[0]>=self.change):
                    self.coin[0] -= self.change
                    self.showcoin[0] = self.change
                    self.change = 0
                else:
                    self.change -= self.coin[0]
                    self.showcoin[0] = self.change
                    self.coin[0] = 0
            self.history_set_list[self.check_pos] = self.item_choose + " Cost :" + str(self.totalpay)
            self.check_pos += 1
            if(self.check_pos>14):
                self.check_pos = 0
            self.list_amount[self.pointer_itemsold] -= 1
            self.list_sell_item[self.pointer_itemsold] += 1
            Machine.price = Machine.price + self.totalpay + self.change
            self.state = 0
            self.cancel()
            self.status()

    def showtext(self):
        self.itemmsg.text = str(self.item_choose)
        self.paymsg.text = str(self.user_paid)
        self.costmsg.text = str(self.totalpay)
        self.changemsg.text = str(self.change)
        self.coin1.text = str(self.showcoin[0])
        self.coin5.text = str(self.showcoin[1])
        self.amountmsg.text = str(self.list_amount[self.pointer_itemsold])

    def showpicture(self):
        name = ""
        if state == 3:
            if(self.pointer_itemsold == 0):
                name = "milk1"
            elif(self.pointer_itemsold == 1):
                name = "milk2"
            elif(self.pointer_itemsold == 2):
                name = "milk3"
            elif(self.pointer_itemsold == 3):
                name = "milk5"
            elif(self.pointer_itemsold == 4):
                name = "milk6"
            elif(self.pointer_itemsold == 5):
                name = "orange1"
        return name

    def status(self):
        if(self.state==0):
            status = "Ready to Use Vending Healthy"
        elif(self.state==1):
            status = "Already Pick Goods Can Spend Money or Cancel"
        elif(self.state==2):
            status = "Paying Can Cancel or Pay Again"
        else:
            status = "Finished Pay Please Keep Goods & Change"
        self.statusmsg.text = status
        
        
class Administrator(Screen):
    
    history = ObjectProperty()
    def addItem(self,types):
        count = types
        if(Machine.list_amount[count]<999):
            Machine.list_amount[count] += 1
        self.showtext()

    def removeItem(self,types):
        count = types
        if(Machine.list_amount[count]>0):
            Machine.list_amount[count] -= 1
        self.showtext()

    def addCoin(self,types):
        count = types
        if(Machine.coin[count]<999):
            Machine.coin[count] += 1
        self.showtext()

    def removeCoin(self,types):
        count = types
        if(Machine.coin[count]>0):
            Machine.coin[count] -= 1
        self.showtext()

    def resetMachine(self):
        Machine.list_amount = [15,15,15,15,15,15]
        Machine.coin = [200,200,0]
        self.showtext()

    def resetSimulator(self):
        Machine.list_amount = [15,15,15,15,15,15]
        Machine.coin = [200,200,0]
        Machine.list_sell_item = [0,0,0,0,0,0]
        Machine.price = 0
        Machine.history_set_list = ["","","","","","","","","","","","","","",""]
        self.showtext()
    
    def showtext(self):
        self.sum.text = str(Machine.coin[0]+(Machine.coin[1]*5)+(Machine.coin[2]*10))
        self.price.text = str(Machine.price)
        self.sellitem1.text = str(Machine.list_sell_item[0])
        self.sellitem2.text = str(Machine.list_sell_item[1])
        self.sellitem3.text = str(Machine.list_sell_item[2])
        self.sellitem4.text = str(Machine.list_sell_item[3])
        self.sellitem5.text = str(Machine.list_sell_item[4])
        self.sellitem6.text = str(Machine.list_sell_item[5])
        self.item1.text = str(Machine.list_amount[0])
        self.item2.text = str(Machine.list_amount[1])
        self.item3.text = str(Machine.list_amount[2])
        self.item4.text = str(Machine.list_amount[3])
        self.item5.text = str(Machine.list_amount[4])
        self.item6.text = str(Machine.list_amount[5])
        self.coin1.text = str(Machine.coin[0])
        self.coin5.text = str(Machine.coin[1])
        self.coin10.text = str(Machine.coin[2])
        self.history.item_strings = Machine.history_set_list

to_open = Builder.load_file("machine.kv")
        
class MachineApp(App):
    def build(self):
        return to_open

if __name__=="__main__":
    MachineApp().run()
